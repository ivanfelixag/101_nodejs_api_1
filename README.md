# Steps to create an API in NodeJS with Express and Typescript
In this file I'm going to explain what are the steps you need to create an API with these technologies.

## Initialize the project
With NodeJS you use the command **npm** to initialize the packege.json file and install the dependencies.

To initialize the project you need to introduce the comand below:

```
npm init
```

That command lets us create the **package.json** file.

### GitIgnore
After that, you need to create the **.gitignore** file with the common files and folders to ignore:
```
# Node JS
node_modules/
.node_modules/
package-lock.json

# MacOs
**/.DS_Store

# Visual Studio Code
coverage
**/.vs
**/.vscode/*
!**/.vscode/tasks.json
!**/.vscode/settings.template.json
!**/.vscode/launch.template.json
!**/.vscode/extensions.json

# IntellJ
.idea

# Yarn
yarn.lock
yarn-error.log

# Linter
.eslintcache
```

## TypeScript

To install TypeScript you need to use **npm** again.

```
npm install typescript -s
```

With the option '-s' you are telling to **npm**: *please, save the depency in the package.json dependencies*.


### Initialize TypeScript
Inside the package.json, create a **script** to call the typescript command.

```
"scripts": {
    "tsc": "tsc"
}
```

Once you have configure the script above, you can excute the command below to initialize TypeScript:
```
npm run tsc -- --init
```

This command create the **tsconfig.json** file with the TypeScript configuration.

### Transpiler
Remember, NodeJS only read JavaScript files (.js). For this reason, you need to transpile the TypeScript files (.ts) to JavaScript. 

You need to uncomment the line **outDir** inside the **tsconfig.json** file to set the folder where the transpiled JavaScript files to be delivered.

```
{
    "compilerOptions": {
        ...
        "outDir": "./dist",
        ...
    }
}
```

Again, to make simply your life, add the below script in your **package.json** file:

```
"scripts": {
    ...
    "build": "rm -r ./dist && npm run tsc"
    ...
}
```

## Express

To install Express you need to use **npm** again.

```
npm install express -s
```

The problem with Express and TypeScript is that they don't know anything about each other. For this reason, you need to make them able to know each other. In this case, you need TypeScript to know about the types of Express.

```
npm install @types/express -s
```

Generate the below project structure:

```
.
└── src
    ├── controllers
    ├── errors
    ├── interfaces
    ├── models
    ├── repositories
    ├── routes
    ├── sanitizers
    ├── services
    ├── validators
    ├── App.ts
    └── index.ts
```