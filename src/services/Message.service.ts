import { IMessage } from "../interfaces/Message.interface";
import config from "../config";
import MessageRepository from "../repositories/MessageRepository";

class MessageService {

  public async retriveMessage(): Promise<IMessage> {
    const result = await MessageRepository.retriveMessageFromPrueba1();

    const message = {
      message: result.message + ' - Axios Prueba 2'
    } as IMessage;

    return message;
  }

}

export default new MessageService();