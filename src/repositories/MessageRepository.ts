import { IMessage } from "../interfaces/Message.interface";
import axios from 'axios';

class MessageRepository {

  public async retriveMessageFromPrueba1(): Promise<IMessage> {
    const result = await axios.get('http://service1.internal-network:8000/prueba1/message');

    return result.data as IMessage;
  }

}

export default new MessageRepository();