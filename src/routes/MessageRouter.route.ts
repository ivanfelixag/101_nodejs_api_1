import { Router } from "express";
import { MessageController }  from '../controllers/Message.controller'

export default class TestRouter {
  private controller: MessageController;
  private router: Router;

  constructor() {
    this.controller = new MessageController();
    this.router = Router();
    this.init();
  }

  getRoutes(): Router {
    return this.router;
  }

  private init(): void {
    this.router.get('/', this.controller.launchMessage.bind(this.controller));
  }

}
