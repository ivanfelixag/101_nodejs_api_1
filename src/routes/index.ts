import { Router } from "express";

import MessageRouter from "./MessageRouter.route";

export default class ApplicationV1Routes {
  private router: Router;

  constructor() {
    this.router = Router();
    this.init();
  }

  public getRoutes(): Router {
    return this.router;
  }

  private init(): void {
    this.router.use("/", new MessageRouter().getRoutes());
    this.router.use("/message", new MessageRouter().getRoutes());
  }
}
